import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import './index.css';
import Home from './Home';
import App from './App';
import ResourcePage from './ResourcePage'
import LoginCallback from './LoginCallback';
//import registerServiceWorker from './registerServiceWorker'
//import App from './App';
import * as serviceWorker from './serviceWorker';

ReactDOM.render(
    
    
  <Router>
    <Switch>
      <Route path="/callback" component={LoginCallback} />
      <Route path="/login" component={Home} />
      <Route path="/" component={App} />
      <Route path="/resource" component={ResourcePage} />
    </Switch>
  </Router>,
  
    document.getElementById('root')
    );

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
//      <Route path="/logout" component={Logout} />
serviceWorker.unregister();
//registerServiceWorker();
