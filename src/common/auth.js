import auth0 from 'auth0-js';

const CLIENT_DOMAIN = 'winniefredbivera.eu.auth0.com';//'__AUTH0_DOMAIN__';
const CLIENT_ID = 'hDoXiXWfCqE83CdW7FfBWdpr4XMTkY2i';//'__AUTH0_CLIENT_ID__';
const SCOPE = 'openid';
const REDIRECT = 'http://localhost:3000/callback';
const HOME = 'http://localhost:3000'
const LIST_OF_TOKEN_TYPES = ["accessToken","appState","expiresIn","idToken","idTokenPayload","refreshToken","scope","state","tokenType"];
//const LOGIN_PAGE = 'http://localhost:3000/login';

const auth = new auth0.WebAuth({
  domain: CLIENT_DOMAIN,
  clientID: CLIENT_ID,
});

export function login() {
  auth.authorize({
    responseType: 'token',
    redirectUri: REDIRECT,
    scope: SCOPE,
  });
}

export function logout() {
  console.log("logging out")
  auth.logout({
      returnTo: HOME
  });
}


function persist_object_in_session_storage(obj,list_of_token_types){
    let list_length = list_of_token_types.length;
    for(let i=0,token_name = list_of_token_types[0]; i<list_length; i++){
        token_name = list_of_token_types[i];
        console.log(token_name +"--" +obj[token_name]);
        sessionStorage.setItem(token_name, obj[token_name]);
    }
}

export function parseTokens(){
  auth.parseHash({ hash: window.location.hash },function(err,answer){
  if (err) {
    return console.log(err);
  }
  console.log(answer);
  try{
  //sessionStorage.clear()
  persist_object_in_session_storage(answer, LIST_OF_TOKEN_TYPES)
  }catch(err){console.log("error while persisting tokens");
      //window.location = LOGIN_PAGE;
  }
})
};

export function checkSession(){
  //  let previousAccessToken = sessionStorage.accessToken;
        auth.checkSession({
        responseType: "token", 
        redirectUri: 'http://localhost:3000/login' 
//        ,accessToken: previousAccessToken
        },
        function(err,authResult){
            console.log("something wrong with token");
            console.log("error from checkSession")
            console.log(err)
    })
    
}
